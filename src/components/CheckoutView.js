import { useContext, useEffect, useState } from "react";
import {
  Container,
  Row,
  Col,
  Table,
  Form,
  Card,
  Button,
} from "react-bootstrap";
import CheckoutData from "./CheckoutData";
import Swal from "sweetalert2";
import UserContext from "../UserContext";
import { Navigate, useNavigate } from "react-router-dom";

export default function CheckoutView({ cartData, fetchData }) {
  const { user } = useContext(UserContext);
  const navigate = useNavigate();
  const [cart, setCart] = useState([]);

  const [userId, setUserId] = useState("");
  const [email, setEmail] = useState("");
  const [firstName, setFirstname] = useState("");
  const [lastName, setLastname] = useState("");
  const [contactNo, setContactNo] = useState("");
  const [address, setAddress] = useState("");

  const totalAmount = cartData.reduce(
    (total, cartItem) => total + cartItem.totalAmount,
    0
  );

  // Initialize cartId to null when cartData is empty
  const cartId = Array.isArray(cartData)
    ? cartData.reduce((total, cartItem) => cartItem._id, 0)
    : null;

  const getUserInfo = () => {
    fetch(`${process.env.REACT_APP_API_URL}/users/retrieve-user-details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        if (typeof data._id !== undefined) {
          setUserId(data._id);
          setEmail(data.email);
          setFirstname(data.firstName);
          setLastname(data.lastName);
          setContactNo(data.contactNo);
          setAddress(data.address);
        }
      });
  };

  useEffect(() => {
    setCart(cartData);
    getUserInfo();
  }, [cartData]);

  const checkout = () => {
    fetch(`${process.env.REACT_APP_API_URL}/orders/${user.id}/checkout`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        cartId: cartId,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        if (data.success) {
          Swal.fire({
            title: "Success",
            icon: "success",
            text: "Cart checkout",
          });

          fetchData();
          navigate("/");
        } else {
          Swal.fire({
            title: "Error",
            icon: "error",
            text: data.message,
          });

          fetchData();
        }
      });
  }; //end of checkout

  return (
    <Container className="mt-5">
      <Row>
        <Col lg={7}>
          <h1>Checkout</h1>

          <h4 className="mt-4">Shipping Details</h4>

          <Table className="table-borderless">
            <tbody>
              <tr>
                <td>Name</td>
                <td>:</td>
                <td>{`${firstName} ${lastName}`}</td>
              </tr>
              <tr>
                <td>Address</td>
                <td>:</td>
                <td>{address}</td>
              </tr>
              <tr>
                <td>Contact</td>
                <td>:</td>
                <td>{contactNo}</td>
              </tr>
            </tbody>
          </Table>

          <hr />

          <h4 className="mt-4">Delivery Type</h4>
          <div className="d-flex column">
            <div className="me-5">
              <div className="mt-4">
                <Form>
                  <Form.Check
                    type="radio"
                    label="Free Shipping  &#8369;0"
                    defaultChecked
                  ></Form.Check>
                </Form>
                <p className="small mt-3 mb-0 fw-bold">
                  Estimated delivery 8-10 days
                </p>
                <p className="small">Get Free Shipped products in Time!</p>
              </div>

              <div className="mt-4">
                <Form>
                  <Form.Check
                    text-muted
                    type="radio"
                    label="Standard Shipping  &#8369;100"
                    checked={false}
                    disabled
                  ></Form.Check>
                </Form>
                <p className="small mt-3 mb-0 fw-bold text-muted">
                  Estimated delivery 5-8 days
                </p>
                <p className="small text-muted">
                  Get timely delivery with economy shipping.
                </p>
              </div>
            </div>

            <div className="ms-5">
              <div className="mt-4">
                <Form>
                  <Form.Check
                    text-muted
                    type="radio"
                    label="2 days Shipping  &#8369;200"
                    checked={false}
                    disabled
                  ></Form.Check>
                </Form>
                <p className="small mt-3 mb-0 fw-bold text-muted">
                  Estimated delivery 2 days
                </p>
                <p className="small text-muted text-muted">
                  Everything faster with minimum shipping fee.
                </p>
              </div>

              <div className="mt-4">
                <Form>
                  <Form.Check
                    text-muted
                    type="radio"
                    label="One day Shipping  &#8369;250"
                    checked={false}
                    disabled
                  ></Form.Check>
                </Form>
                <p className="small mt-3 mb-0 fw-bold text-muted">
                  Estimated delivery 1 day
                </p>
                <p className="small text-muted">
                  Highest priority shipping at the lowest cost.
                </p>
              </div>
            </div>
          </div>
        </Col>

        <Col className="mt-5 mb-5" lg={5}>
          <Card>
            <Card.Body>
              <h4 className="fw-bold p-3">Summary</h4>
              {cart.map((cartItems) => (
                <CheckoutData key={cartItems._id} cartProp={cartItems} />
              ))}
              <div className="d-flex column">
                <h4 className="me-auto">Total:</h4>
                <h4 className="text-danger">
                  &#8369;{totalAmount.toLocaleString()}
                </h4>
              </div>
              <div className="d-flex">
                <Button
                  className="ms-auto"
                  variant="danger"
                  block
                  onClick={checkout}
                >
                  Pay Now
                </Button>
              </div>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
} //end of CheckoutView
