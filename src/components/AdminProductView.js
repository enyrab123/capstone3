import { useState, useEffect } from "react";
import { Table } from "react-bootstrap";
import EditProduct from "./EditProduct";
import ArchiveProduct from "./ArchiveProduct";
import AddProduct from "./AddProduct";

export default function AdminProductView({ productsData, fetchData }) {
  const [products, setProducts] = useState([]);

  useEffect(() => {
    updateProductsTable(productsData);
  }, [productsData]);

  const updateProductsTable = (data) => {
    const productsTable = data.map((product) => (
      <tr key={product._id}>
        <td>{product._id}</td>
        <td>{product.name}</td>
        <td>{product.description}</td>
        <td className="text-danger">&#8369;{product.price.toLocaleString()}</td>
        <td className={product.isActive ? "text-success" : "text-danger"}>
          {product.isActive ? "Available" : "Unavailable"}
        </td>
        <td>
          <EditProduct product={product._id} fetchData={fetchData} />
        </td>
        <td>
          <ArchiveProduct
            product={product._id}
            isActive={product.isActive}
            fetchData={fetchData}
          />
        </td>
      </tr>
    ));

    setProducts(productsTable);
  };

  const productsTable = (
    <Table striped bordered hover responsive>
      <thead>
        <tr className="text-center">
          <th>ID</th>
          <th>Name</th>
          <th>Description</th>
          <th>Price</th>
          <th>Availability</th>
          <th colSpan="2">Actions</th>
        </tr>
      </thead>
      <tbody>{products}</tbody>
    </Table>
  );

  return (
    <>
      <h1 className="text-center my-4">Products Panel</h1>
      {productsTable}
      <AddProduct fetchData={fetchData} />
    </>
  );
}
