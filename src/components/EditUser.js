import { useState } from "react";
import { Button, Modal, Form } from "react-bootstrap";
import Swal from "sweetalert2";

export default function EditUser({ user, getUsers }) {
  const [userId, setUserId] = useState("");

  const [email, setEmail] = useState("");
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [contactNo, setContactNo] = useState("");
  const [address, setAddress] = useState("");

  const [showEdit, setShowEdit] = useState(false);
  console.log(user);

  const openEdit = (userId) => {
    fetch(`${process.env.REACT_APP_API_URL}/users/retrieve-user-details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setUserId(data._id);
        setEmail(data.email);
        setFirstName(data.firstName);
        setLastName(data.lastName);
        setContactNo(data.contactNo);
        setAddress(data.address);
      });

    //Then, open the modal
    setShowEdit(true);
  }; // end of openEdit

  const closeEdit = () => {
    setShowEdit(false);
    setEmail("");
    setFirstName("");
    setLastName("");
    setContactNo("");
    setAddress("");
  }; //end of closeEdit

  const editUser = (e, userId) => {
    e.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/users/${userId}/update-user-info`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        email: email,
        firstName: firstName,
        lastName: lastName,
        contactNo: contactNo,
        address: address,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        if (data === true) {
          Swal.fire({
            title: "Success!",
            icon: "success",
            text: "User Successfully Updated",
          });

          closeEdit();
          getUsers();
        } else {
          Swal.fire({
            title: "Error!",
            icon: "error",
            text: "Please try again",
          });

          closeEdit();
          getUsers();
        }
      });
  };

  return (
    <>
      <Button variant="primary" size="sm" onClick={() => openEdit(user)}>
        {" "}
        Edit{" "}
      </Button>

      {/*Edit Modal Forms*/}
      <Modal show={showEdit} onHide={closeEdit}>
        <Form onSubmit={(e) => editUser(e, userId)}>
          <Modal.Header closeButton>
            <Modal.Title>Edit User</Modal.Title>
          </Modal.Header>

          <Modal.Body>
            <Form.Group>
              <Form.Label>Email</Form.Label>
              <Form.Control
                type="email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                required
              />
            </Form.Group>

            <Form.Group>
              <Form.Label>First Name</Form.Label>
              <Form.Control
                type="text"
                value={firstName}
                onChange={(e) => setFirstName(e.target.value)}
                required
              />
            </Form.Group>

            <Form.Group>
              <Form.Label>Last Name</Form.Label>
              <Form.Control
                type="text"
                value={lastName}
                onChange={(e) => setLastName(e.target.value)}
                required
              />
            </Form.Group>

            <Form.Group>
              <Form.Label>Contact No</Form.Label>
              <Form.Control
                type="text"
                value={contactNo}
                onChange={(e) => setContactNo(e.target.value)}
                required
              />
            </Form.Group>

            <Form.Group>
              <Form.Label>Address</Form.Label>
              <Form.Control
                type="text"
                value={address}
                onChange={(e) => setAddress(e.target.value)}
                required
              />
            </Form.Group>
          </Modal.Body>

          <Modal.Footer>
            <Button variant="secondary" onClick={closeEdit}>
              Close
            </Button>
            <Button variant="success" type="submit">
              Submit
            </Button>
          </Modal.Footer>
        </Form>
      </Modal>
    </>
  ); //end of return
} //end of EditUser
