import { Button } from "react-bootstrap";
import Swal from "sweetalert2";

export default function SetOrderStatus({ order, orderStatus, fetchData }) {
  const ongoingToggle = (e, orderId) => {
    e.preventDefault();

    fetch(
      `${process.env.REACT_APP_API_URL}/orders/${orderId}/set-order-ongoing`,
      {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      }
    )
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        if (data.success === true) {
          Swal.fire({
            title: "Success",
            icon: "success",
            text: data.message,
          });
          fetchData();
        } else {
          Swal.fire({
            title: "Something Went Wrong",
            icon: "error",
            text: data.message,
          });
          fetchData();
        }
      });
  }; //end of SetOrderStatus

  const completedToggle = (e, orderId) => {
    e.preventDefault();

    fetch(
      `${process.env.REACT_APP_API_URL}/orders/${orderId}/set-order-completed`,
      {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      }
    )
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        if (data.success === true) {
          Swal.fire({
            title: "Success",
            icon: "success",
            text: data.message,
          });
          fetchData();
        } else {
          Swal.fire({
            title: "Something Went Wrong",
            icon: "error",
            text: data.message,
          });
          fetchData();
        }
      });
  }; // end of completedToggle

  return (
    <>
      {orderStatus == "Ongoing" ? (
        <Button
          variant="warning"
          size="sm"
          onClick={(e) => completedToggle(e, order)}
        >
          Ongoing
        </Button>
      ) : (
        <Button
          variant="success"
          size="sm"
          onClick={(e) => ongoingToggle(e, order)}
        >
          Completed
        </Button>
      )}
    </>
  );
} //end of SetUserRole
