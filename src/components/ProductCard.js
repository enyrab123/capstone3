import PropTypes from "prop-types";
import { useContext } from "react";
import { Link } from "react-router-dom";
import { Card, Button } from "react-bootstrap";
import { BsFillCartPlusFill } from "react-icons/bs";
import UserContext from "../UserContext";
import Swal from "sweetalert2";

export default function ProductCard({ productProp }) {
  const { user } = useContext(UserContext);
  const { _id, name, price, image } = productProp;

  console.log(name);

  const addToCart = () => {
    fetch(`${process.env.REACT_APP_API_URL}/cart/${user.id}/add-to-cart`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        products: [
          {
            name: name,
            productId: _id,
            quantity: 1,
          },
        ],
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        if (data.success) {
          Swal.fire({
            title: "Success",
            icon: "success",
            text: `${name} is added to cart`,
          });
        } else {
          Swal.fire({
            title: "Error",
            icon: "error",
            text: data.message,
          });
        }
      });
  };

  return (
    <>
      <Link className="remove-link-card" to={`/products/${_id}`}>
        <Card
          style={{ width: "20rem", cursor: "pointer" }}
          className="h-100 w-100"
        >
          <Card.Img variant="top" src={image} />
          <Card.Body className="d-flex flex-column">
            <div>
              <Card.Title>{name}</Card.Title>
              <h5 className="text-danger">&#8369;{price.toLocaleString()}</h5>
              <p className="mb-0">Shipping cost: Free</p>
            </div>

            <div className="d-flex flex-grow-1 flex-column justify-content-between">
              <div className="d-flex column">
                <p>Stock: </p>
                <p className="text-success ms-1">Available</p>
              </div>

              <div className="d-flex column justify-content-between align-items-center">
                <Link
                  addToCart={addToCart}
                  className="btn btn-sm btn-danger"
                  to={`/products/${_id}`}
                >
                  Details
                </Link>

                <Button
                  className="btn btn-danger"
                  onClick={(e) => {
                    e.preventDefault(); // Prevent the default link behavior
                    addToCart();
                  }}
                >
                  <BsFillCartPlusFill />
                </Button>
              </div>
            </div>
          </Card.Body>
        </Card>
      </Link>
    </>
  );
} // end of function ProductCard

ProductCard.propTypes = {
  // The "shape" method is used to check if a prop object conforms to a specific "shape"
  product: PropTypes.shape({
    // Define the properties and their expected types
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    image: PropTypes.string.isRequired,
  }),
};
