import { useEffect, useState } from "react";
import { Col, Container, Row } from "react-bootstrap";
import PreviewFeaturedProduct from "./PreviewFeaturedProduct";

export default function Featuredproducts() {
  const [previews, setPreviews] = useState([]);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/retrieve-active-products`)
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        const numbers = [];
        const featured = [];

        const generateRandomNums = () => {
          let randomNum = Math.floor(Math.random() * data.length);

          if (numbers.indexOf(randomNum) === -1) {
            numbers.push(randomNum);
          } else {
            generateRandomNums();
          }
        }; //end of generateRandomNums

        let productToFeature = 0;

        if (data.length < 6) {
          productToFeature = data.length;
        } else {
          productToFeature = 6;
        }

        for (let x = 0; x < productToFeature; x++) {
          generateRandomNums();

          featured.push(
            <Col key={data.id} sm={12} md={6} lg={4} className="mb-4">
              <PreviewFeaturedProduct
                data={data[numbers[x]]}
                key={data[numbers[x]]._id}
                breakPoint={3}
              />
            </Col>
          );
        }

        setPreviews(featured);
      });
  }, []);

  return (
    <>
      <h2 className="text-center mt-5">Featured Products</h2>
      <Container fluid className="mt-3 p-0">
        <Row>{previews}</Row>
      </Container>
    </>
  );
} // end of Featuredproducts
