import { useEffect, useState } from "react";
import { Table } from "react-bootstrap";
import SetUserRole from "./SetUserRole";

export default function UserView({ usersData, getUsers }) {
  const [users, setUsers] = useState([]);

  useEffect(() => {
    if (Array.isArray(usersData)) {
      setUsers(usersData);
    } else {
      setUsers([]);
    }
  }, [usersData]);

  return (
    <>
      <h1 className="text-center my-4">Users Panel</h1>

      <Table striped bordered hover responsive>
        <thead>
          <tr className="text-center">
            <th className="text-center">ID</th>
            <th>Email</th>
            <th>First Name</th>
            <th>Last Name</th> {/* Fixed typo here */}
            <th>Contact Number</th>
            <th>Address</th>
            <th>Joined Date</th>
            <th>Role</th>
          </tr>
        </thead>

        <tbody>
          {users.map((user) => (
            <tr key={user._id}>
              <td>{user._id}</td>
              <td>{user.email}</td>
              <td>{user.firstName}</td>
              <td>{user.lastName}</td>
              <td>{user.contactNo}</td>
              <td>{user.address}</td>
              <td>
                {new Date(user.joinedDate).toLocaleDateString("en-US", {
                  year: "numeric",
                  month: "2-digit",
                  day: "2-digit",
                })}
              </td>
              <td className={user.isAdmin ? "text-success" : "text-secondary"}>
                {user.isAdmin ? "Admin" : "Regular"}
              </td>
              <td>
                <SetUserRole
                  user={user._id}
                  isAdmin={user.isAdmin}
                  getUsers={getUsers}
                />
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
    </>
  );
}
