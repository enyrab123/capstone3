import { useState } from "react";
import { InputGroup } from "react-bootstrap";

export default function UpdateQuantity({
  propQuantity,
  propCartId,
  propProductId,
  fetchData,
}) {
  const [quantity, setQuantity] = useState(propQuantity);
  const [isChangingQuantity, setIsChangingQuantity] = useState(false);

  const onIncrement = () => {
    const newQuantity = quantity + 1; // Calculate the new quantity
    setQuantity(newQuantity);
    setIsChangingQuantity(true);

    changeQuantity(newQuantity).then(() => {
      setIsChangingQuantity(false);
      fetchData();
    });
  };

  const onDecrement = () => {
    if (quantity > 1) {
      const newQuantity = quantity - 1; // Calculate the new quantity
      setQuantity(newQuantity);
      setIsChangingQuantity(true);

      changeQuantity(newQuantity).then(() => {
        setIsChangingQuantity(false);
        fetchData();
      });
    }
  };

  const changeQuantity = async (newQuantity) => {
    try {
      const response = await fetch(
        `${process.env.REACT_APP_API_URL}/cart/${propCartId}/change-quantity`,
        {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
          body: JSON.stringify({
            products: [
              {
                productId: propProductId,
                quantity: newQuantity, // Use the new quantity here
              },
            ],
          }),
        }
      );

      if (response.ok) {
        const data = await response.json();
        console.log(data);
      } else {
        // Handle error here
        console.error("Failed to update quantity:", response.statusText);
      }
    } catch (error) {
      // Handle any network or other errors
      console.error("An error occurred while updating quantity:", error);
    }
  };

  return (
    <div>
      <div className="d-flex flex-column align-items-center">
        <h6 className="mb-0">Quantity</h6>
        <div className="d-flex align-items-center mt-1">
          <InputGroup>
            <button
              onClick={onDecrement}
              type="button"
              className={`btn btn-sm btn-outline-secondary px-2 ${
                isChangingQuantity && "disabled"
              }`}
            >
              -
            </button>
            <InputGroup.Text id="basic-addon1">{quantity}</InputGroup.Text>
            <button
              onClick={onIncrement}
              type="button"
              className={`btn btn-sm btn-outline-secondary px-2 ${
                isChangingQuantity && "disabled"
              }`}
            >
              +
            </button>
          </InputGroup>
        </div>
      </div>
    </div>
  );
}
