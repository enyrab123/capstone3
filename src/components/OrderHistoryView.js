import React, { useState, useEffect } from "react";
import OrderHistoryCard from "./OrderHistoryCard";

export default function OrderHistoryView({ orderData, fetchData }) {
  const [orders, setOrders] = useState([]);

  useEffect(() => {
    setOrders(orderData);
  }, [orderData]);

  console.log(orders);

  return (
    <div className="mb-5">
      {orders.map((order) => (
        <OrderHistoryCard
          key={order._id}
          orderProp={order}
          fetchData={fetchData}
        />
      ))}
    </div>
  );
}
