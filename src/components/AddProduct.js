import { useState, useContext } from "react";
import { Form, Button, Modal } from "react-bootstrap";
import { Navigate } from "react-router-dom";
import Swal from "sweetalert2";
import UserContext from "../UserContext";

export default function AddProduct({ fetchData }) {
  const { user } = useContext(UserContext);

  //input states
  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState("");
  const [image, setImage] = useState("");

  const [showModal, setShowModal] = useState(false);

  const openModal = () => {
    setShowModal(true);
  };

  const closeModal = () => {
    setShowModal(false);
  };

  function createProduct(e) {
    e.preventDefault();

    let token = localStorage.getItem("token");

    fetch(`${process.env.REACT_APP_API_URL}/products/create-product`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify({
        name: name,
        description: description,
        price: price,
        image: image,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        if (data.success) {
          Swal.fire({
            title: "Success",
            icon: "success",
            text: data.message,
          });

          fetchData();
          closeModal();
        } else {
          Swal.fire({
            title: "Error",
            icon: "error",
            text: data.message,
          });

          fetchData();
          closeModal();
        }
      });

    setName("");
    setDescription("");
    setPrice(0);
    setImage("");
  } // end of createProduct

  return user.isAdmin === true ? (
    <>
      <div className="text-center mb-5">
        <Button variant="danger" onClick={() => openModal()}>
          {" "}
          Add Product{" "}
        </Button>
      </div>

      {/*Edit Modal Forms*/}
      <Modal show={showModal} onHide={closeModal}>
        <Form onSubmit={(e) => createProduct(e)}>
          <Modal.Header closeButton>
            <Modal.Title>Add Product</Modal.Title>
          </Modal.Header>

          <Modal.Body>
            <Form.Group>
              <Form.Label>Name</Form.Label>
              <Form.Control
                type="text"
                value={name}
                onChange={(e) => setName(e.target.value)}
                required
              />
            </Form.Group>

            <Form.Group>
              <Form.Label>Description</Form.Label>
              <Form.Control
                type="text"
                value={description}
                onChange={(e) => setDescription(e.target.value)}
                required
              />
            </Form.Group>

            <Form.Group>
              <Form.Label>Price</Form.Label>
              <Form.Control
                type="number"
                value={price}
                onChange={(e) => setPrice(e.target.value)}
                required
              />
            </Form.Group>

            <Form.Group>
              <Form.Label>Image</Form.Label>
              <Form.Control
                placeholder="Link an online image"
                type="text"
                value={image}
                onChange={(e) => setImage(e.target.value)}
              />
            </Form.Group>
          </Modal.Body>

          <Modal.Footer>
            <Button variant="secondary" onClick={closeModal}>
              Close
            </Button>
            <Button variant="danger" type="submit">
              Submit
            </Button>
          </Modal.Footer>
        </Form>
      </Modal>
    </>
  ) : (
    <Navigate to="/products" />
  ); // end of return
} // end of function AddProduct
