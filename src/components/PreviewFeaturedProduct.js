import { Link } from "react-router-dom";
import { Card } from "react-bootstrap";

export default function Product(props) {
  const { data } = props;
  const { _id, name, price, image } = data;

  const scrollToTop = () => {
    window.scrollTo({ top: 0, behavior: "smooth" });
  };

  return (
    <>
      <Link
        className="remove-link-card"
        to={`/products/${_id}`}
        onClick={scrollToTop}
      >
        <Card style={{ width: "20rem" }} className="h-100 w-100">
          <Card.Img variant="top" src={image} />
          <Card.Body className="d-flex flex-column">
            <div>
              <Card.Title>{name}</Card.Title>
              <h5 className="text-danger">&#8369;{price.toLocaleString()}</h5>
              <p className="mb-0">Shipping cost: Free</p>
            </div>

            <div className="d-flex flex-grow-1 flex-column justify-content-between">
              <div className="d-flex column">
                <p>Stock: </p>
                <p className="text-success ms-1">Available</p>
              </div>

              <div className="d-flex column justify-content-between align-items-center">
                <Link
                  className="btn btn-sm btn-danger"
                  to={`/products/${_id}`}
                  onClick={scrollToTop}
                >
                  Details
                </Link>
              </div>
            </div>
          </Card.Body>
        </Card>
      </Link>
    </>
  );
} // end of function Product
