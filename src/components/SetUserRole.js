import { Button } from "react-bootstrap";
import Swal from "sweetalert2";

export default function SetUserRole({ user, isAdmin, getUsers }) {
  console.log(isAdmin);
  const adminToggle = (e, userId) => {
    e.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/users/${userId}/set-as-admin`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        if (data.success === true) {
          Swal.fire({
            title: "Success",
            icon: "success",
            text: data.message,
          });
          getUsers();
        } else {
          Swal.fire({
            title: "Something Went Wrong",
            icon: "error",
            text: data.message,
          });
          getUsers();
        }
      });
  };

  const nonAdminToggle = (e, userId) => {
    e.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/users/${userId}/set-as-non-admin`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        if (data.success === true) {
          Swal.fire({
            title: "Success",
            icon: "success",
            text: data.message,
          });
          getUsers();
        } else {
          Swal.fire({
            title: "Something Went Wrong",
            icon: "error",
            text: data.message,
          });
          getUsers();
        }
      });
  }; //end of nonAdminToggle

  return (
    <>
      {isAdmin ? (
        <Button
          variant="secondary"
          size="sm"
          onClick={(e) => nonAdminToggle(e, user)}
        >
          Regular
        </Button>
      ) : (
        <Button
          variant="success"
          size="sm"
          onClick={(e) => adminToggle(e, user)}
        >
          Admin
        </Button>
      )}
    </>
  );
} //end of SetUserRole
