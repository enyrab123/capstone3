import { useState } from "react";
import { Container, Col, Row, Image, Carousel } from "react-bootstrap";

import img1 from "../img/img1.webp";
import img2 from "../img/img2.webp";
import img3 from "../img/img3.webp";
import img4 from "../img/img4.webp";

import sideImg1 from "../img/side-img1.png";
import sideImg2 from "../img/side-img2.png";
import sideImg3 from "../img/side-img3.webp";
import sideImg4 from "../img/side-img4.webp";

export default function Advertisement() {
  const [index, setIndex] = useState(0);

  const handleSelect = (selectedIndex) => {
    setIndex(selectedIndex);
  };

  return (
    <Container>
      <Row className="justify-content-center mt-5">
        <Col lg={7}>
          <div className="responsive-carousel">
            <Carousel activeIndex={index} onSelect={handleSelect}>
              <Carousel.Item>
                <Image fluid src={img1} />
              </Carousel.Item>

              <Carousel.Item>
                <Image fluid src={img2} />
              </Carousel.Item>

              <Carousel.Item>
                <Image fluid src={img3} />
              </Carousel.Item>

              <Carousel.Item>
                <Image fluid src={img4} />
              </Carousel.Item>
            </Carousel>
          </div>
        </Col>
      </Row>
      <Row className="mt-3">
        <Col className="text-center">
          <Image fluid src={sideImg1} />
          <Image fluid src={sideImg2} />
          <Image fluid src={sideImg3} />
          <Image fluid src={sideImg4} />
        </Col>
      </Row>
    </Container>
  );
} //end of Advertisement
