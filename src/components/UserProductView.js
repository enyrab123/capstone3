import React, { useState, useEffect } from "react";
import { Col, Container, Row } from "react-bootstrap";
import ProductCard from "./ProductCard";

export default function UserProductView({ productsData }) {
  const [products, setProducts] = useState([]);

  useEffect(() => {
    const productArr = productsData.map((product) => {
      if (product.isActive === true) {
        return (
          <Col key={product.id} sm={12} md={6} lg={4} className="mb-4">
            <ProductCard productProp={product} />
          </Col>
        );
      } else {
        return null;
      }
    }); // end of map

    setProducts(productArr);
  }, [productsData]);

  return (
    <Container fluid className="mt-5">
      <Row>{products}</Row>
    </Container>
  );
} // end of function UserProductView
