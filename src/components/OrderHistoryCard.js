import React from "react";
import { Badge, Col, Container, Row } from "react-bootstrap";
import { useContext } from "react";
import UserContext from "../UserContext";
import SetOrderStatus from "./SetOrderStatus";

export default function OrderHistoryCard({ orderProp, fetchData }) {
  const { _id, totalAmount, purchasedOn, orderStatus } = orderProp;
  const { user } = useContext(UserContext);

  console.log(orderStatus);

  return (
    <Container className="border mt-3">
      <Row className="p-3">
        <Col lg={3} className="d-flex align-items-center">
          <div>
            <h6 className="mb-0 fw-bold">Order ID:</h6>
            <p className="mb-0 small">{_id}</p>
          </div>
        </Col>

        <Col lg={3} className="d-flex align-items-center">
          <div>
            <h6 className="mb-0 fw-bold">Total Amount: </h6>
            <p className="mb-0 text-danger small">
              &#8369;{totalAmount.toLocaleString()}
            </p>
          </div>
        </Col>
        <Col lg={3} className="d-flex align-items-center">
          <div>
            <h6 className="mb-0 fw-bold">Purchased On:</h6>
            <p className="mb-0 small">
              {new Date(purchasedOn).toLocaleString()}
            </p>
          </div>
        </Col>

        <Col lg={3} className="d-flex align-items-center">
          <div>
            <h6 className="mb-0 fw-bold">Status:</h6>
            {user.isAdmin === true ? (
              <SetOrderStatus
                order={orderProp._id}
                orderStatus={orderStatus}
                fetchData={fetchData}
              />
            ) : orderStatus === "Completed" ? (
              <Badge bg="success">{orderStatus}</Badge>
            ) : (
              <Badge bg="warning">{orderStatus}</Badge>
            )}
          </div>
        </Col>
      </Row>
    </Container>
  );
}
