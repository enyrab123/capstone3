import PropTypes from "prop-types";
import { Col, Row, Image } from "react-bootstrap";
import Swal from "sweetalert2";
import UpdateQuantity from "./UpdateQuantity";

export default function CartCard({ cartProp, cartIdProp, fetchData }) {
  const { productId, name, subTotal, image, quantity, price } = cartProp;

  const removeItem = () => {
    fetch(
      `${process.env.REACT_APP_API_URL}/cart/${cartIdProp}/${productId}/remove-product`,
      {
        method: "DELETE",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      }
    )
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        if (data.success) {
          Swal.fire({
            title: "Success",
            icon: "success",
            text: `${name} is removed`,
          });

          fetchData();
        } else {
          Swal.fire({
            title: "Error",
            icon: "error",
            text: data.message,
          });

          fetchData();
        }
      });
  }; //end of removeItem

  return (
    <Row className="border mt-3">
      <Col lg={2} md={3} sm={4} className="p-3">
        <Image style={{ width: "9rem" }} fluid src={image} />
      </Col>

      <Col
        lg={4}
        md={3}
        sm={8}
        xs={6}
        className="p-3 d-flex align-items-center"
      >
        <div>
          <h6 className="mb-0">Product:</h6>
          <p className="mb-0">{name}</p>
          <p className="text-danger mb-0 remove-link" onClick={removeItem}>
            Remove
          </p>
        </div>
      </Col>

      <Col lg={2} md={2} sm={4} className="p-3 d-flex align-items-center">
        <div>
          <h6 className="mb-0">Price:</h6>
          <p className="mb-0 text-danger">&#8369;{price.toLocaleString()}</p>
        </div>
      </Col>

      <Col lg={2} md={2} sm={4} className="p-3 d-flex align-items-center">
        <UpdateQuantity
          propQuantity={quantity}
          propCartId={cartIdProp}
          propProductId={productId}
          fetchData={fetchData}
        />
      </Col>

      <Col lg={2} md={2} sm={4} className="p-3 d-flex align-items-center">
        <div>
          <h6 className="mb-0">Subtotal</h6>
          <p className="mb-0 text-danger">&#8369;{subTotal.toLocaleString()}</p>
        </div>
      </Col>
    </Row>
  ); //end of return
} //end of function CartCard

CartCard.propTypes = {
  // The "shape" method is used to check if a prop object conforms to a specific "shape"
  cart: PropTypes.shape({
    // Define the properties and their expected types
    name: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    image: PropTypes.string.isRequired,
  }),
};
