import { Col, Container, Row, Image } from "react-bootstrap";

export default function CheckoutData({ cartProp }) {
  return (
    <Container>
      {cartProp.products.map((product, index) => (
        <Row key={index}>
          <Col lg={4} md={4} sm={12} className="d-flex p-0 align-items-center">
            <div style={{ width: "5rem", height: "5rem" }}>
              <Image fluid src={product.image} />
            </div>
          </Col>

          <Col lg={4} md={4} sm={12} className="d-flex align-items-center">
            <p className="small">{product.name}</p>
          </Col>

          <Col lg={2} md={2} sm={6} className="d-flex align-items-center">
            <p className="small">x{product.quantity}</p>
          </Col>

          <Col lg={2} md={2} sm={6} className="d-flex align-items-center">
            <p className="small">&#8369;{product.subTotal.toLocaleString()}</p>
          </Col>
        </Row>
      ))}
      <hr />
    </Container>
  );
}
