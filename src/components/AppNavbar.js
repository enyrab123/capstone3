import { useContext } from "react";
import { Container, NavDropdown } from "react-bootstrap";
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";

import { Link, NavLink } from "react-router-dom";
import UserContext from "../UserContext";

export default function AppNavbar() {
  const { user } = useContext(UserContext);

  return (
    <Navbar bg="danger" expand="lg">
      <Container fluid>
        <Navbar.Brand className="font-logo text-muted" as={Link} to="/">
          The Gadget Grid
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="ms-auto">
            {user.id !== null ? (
              user.isAdmin === true ? (
                <>
                  <Nav.Link as={NavLink} to="/products" exact>
                    Products
                  </Nav.Link>
                  <Nav.Link as={NavLink} to="/users" exact>
                    Users
                  </Nav.Link>
                  <Nav.Link as={NavLink} to="/orders" exact>
                    All Purchases
                  </Nav.Link>
                  <Nav.Link as={Link} to="/logout">
                    Logout
                  </Nav.Link>
                </>
              ) : (
                <>
                  <Nav.Link as={NavLink} to="/" exact>
                    Home
                  </Nav.Link>
                  <Nav.Link as={NavLink} to="/products" exact>
                    Products
                  </Nav.Link>
                  <Nav.Link as={NavLink} to="/:userId/retrieve-cart" exact>
                    Cart
                  </Nav.Link>

                  <NavDropdown title="Account" id="basic-nav-dropdown">
                    <NavDropdown.Item as={Link} to="/profile">
                      Profile
                    </NavDropdown.Item>
                    <NavDropdown.Item as={NavLink} to="/orders" exact>
                      Purchases
                    </NavDropdown.Item>
                  </NavDropdown>
                  <Nav.Link as={Link} to="/logout">
                    Logout
                  </Nav.Link>
                </>
              )
            ) : (
              <>
                <Nav.Link as={NavLink} to="/" exact>
                  Home
                </Nav.Link>
                <Nav.Link as={Link} to="/login">
                  Login
                </Nav.Link>
                <Nav.Link as={Link} to="/register">
                  Register
                </Nav.Link>
              </>
            )}
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  ); // end of return
} // end of AppNavbar
