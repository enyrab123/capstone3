import { Button } from "react-bootstrap";
import Swal from "sweetalert2";

export default function ArchiveProduct({ product, isActive, fetchData }) {
  const archiveToggle = (e, productId) => {
    e.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/archive`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        if (data.success === true) {
          Swal.fire({
            title: "Success",
            icon: "success",
            text: data.message,
          });
          fetchData();
        } else {
          Swal.fire({
            title: "Something Went Wrong",
            icon: "error",
            text: data.message,
          });
          fetchData();
        }
      });
  }; // end of archiveToggle

  const activateToggle = (e, productId) => {
    e.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/activate`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        if (data.success === true) {
          Swal.fire({
            title: "Success",
            icon: "success",
            text: data.message,
          });
          fetchData();
        } else {
          Swal.fire({
            title: "Something Went Wrong",
            icon: "error",
            text: data.message,
          });
          fetchData();
        }
      });
  }; // end of activateToggle

  return (
    <>
      {isActive ? (
        <Button
          variant="secondary"
          size="sm"
          onClick={(e) => archiveToggle(e, product)}
        >
          Archive
        </Button>
      ) : (
        <Button
          variant="success"
          size="sm"
          onClick={(e) => activateToggle(e, product)}
        >
          Activate
        </Button>
      )}
    </>
  ); // end of return
} // end of ArchiveProduct
