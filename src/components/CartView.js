import { useEffect, useState, useContext } from "react";
import { Button, Container } from "react-bootstrap";
import Swal from "sweetalert2";
import UserContext from "../UserContext";
import CartCard from "./CartCard";
import { Link } from "react-router-dom";

export default function CartView({ cartData, fetchData }) {
  const { user } = useContext(UserContext);
  const [cart, setCart] = useState(cartData);

  // Initialize cartId to null when cartData is empty
  const cartId = Array.isArray(cart)
    ? cart.reduce((total, cartItem) => cartItem._id, 0)
    : null;

  // Initialize totalAmount to 0 when cartData is empty
  const totalAmount = Array.isArray(cart)
    ? cart.reduce((total, cartItem) => total + cartItem.totalAmount, 0)
    : 0;

  useEffect(() => {
    setCart(cartData);
  }, [cartData]);

  const clearCart = () => {
    fetch(`${process.env.REACT_APP_API_URL}/cart/${cartId}/clear-cart`, {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        if (data.success) {
          Swal.fire({
            title: "Success",
            icon: "success",
            text: "Cart is cleared",
          });
          fetchData();
        } else {
          Swal.fire({
            title: "Error",
            icon: "error",
            text: data.message,
          });
          fetchData();
        }
      });
  };

  return (
    <>
      {totalAmount !== 0 ? (
        <div>
          {cart.map((cartItem, index) => (
            <Container key={index}>
              {cartItem.products.map((product) => (
                <CartCard
                  cartProp={product}
                  cartIdProp={cartItem._id}
                  fetchData={fetchData}
                  key={product.productId} // Assuming productId is a unique identifier
                />
              ))}
            </Container>
          ))}
          <div className="text-end mt-3">
            <h6>
              Total Amount:{" "}
              <span className="text-danger">
                &#8369;{totalAmount.toLocaleString()}
              </span>
            </h6>

            <Link className="btn btn-sm btn-danger btn-block" to={`/checkout`}>
              Checkout
            </Link>

            <Button
              className="mx-2 btn-sm"
              variant="danger"
              block
              onClick={clearCart}
            >
              Clear Cart
            </Button>
          </div>
        </div>
      ) : (
        <h1 className="text-center mt-5">Cart is empty</h1>
      )}
    </>
  );
}
