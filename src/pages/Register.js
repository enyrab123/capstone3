import { useState, useEffect, useContext } from "react";
import {
  Form,
  Button,
  Container,
  Row,
  Col,
  Card,
  Image,
} from "react-bootstrap";
import { Navigate, useNavigate } from "react-router-dom";
import UserContext from "../UserContext";
import Swal from "sweetalert2";

import registerImg from "../img/register-img.png";

export default function Register() {
  const { user } = useContext(UserContext);
  const navigate = useNavigate();

  // State hooks to store the values of the input fields
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email, setEmail] = useState("");
  const [contactNo, setContactNo] = useState("");
  const [address, setAddress] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  // State to determine whether submit button is enabled or not
  const [isActive, setIsActive] = useState(false);

  // Check if values are successfully binded
  console.log(firstName);
  console.log(lastName);
  console.log(email);
  console.log(contactNo);
  console.log(address);
  console.log(password);
  console.log(confirmPassword);

  async function registerUser(e) {
    e.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/users/registration`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        firstName: firstName,
        lastName: lastName,
        email: email,
        contactNo: contactNo,
        address: address,
        password: password,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        if (data === true) {
          setFirstName("");
          setLastName("");
          setEmail("");
          setContactNo("");
          setAddress("");
          setPassword("");
          setConfirmPassword("");

          Swal.fire({
            title: "Register Successful",
            icon: "success",
            text: "We are happy to have you!",
          });

          navigate("/login");
        } else {
          Swal.fire({
            title: "Error",
            icon: "error",
            text: data.message,
          });
        }
      });
  } // end of function registerUser

  useEffect(() => {
    if (
      firstName !== "" &&
      lastName !== "" &&
      email !== "" &&
      contactNo !== "" &&
      address !== "" &&
      password !== "" &&
      confirmPassword !== "" &&
      password === confirmPassword &&
      contactNo.length === 11
    ) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [
    firstName,
    lastName,
    email,
    contactNo,
    address,
    password,
    confirmPassword,
  ]);

  return user.id !== null ? (
    <Navigate to="/login" />
  ) : (
    <Container className="mt-5">
      <Row className="justify-content-center align-items-center">
        <Col lg={5}>
          <Image src={registerImg} fluid />
        </Col>

        <Col lg={4} className="mx-5">
          <Card className="border-0 p-3">
            <Form onSubmit={(e) => registerUser(e)}>
              <h1 className="my-5 text-center">Register</h1>
              <Form.Group>
                <Form.Label>First Name:</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Enter First Name"
                  required
                  value={firstName}
                  onChange={(e) => {
                    setFirstName(e.target.value);
                  }}
                />
              </Form.Group>
              <Form.Group>
                <Form.Label>Last Name:</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Enter Last Name"
                  required
                  value={lastName}
                  onChange={(e) => {
                    setLastName(e.target.value);
                  }}
                />
              </Form.Group>
              <Form.Group>
                <Form.Label>Email:</Form.Label>
                <Form.Control
                  type="email"
                  placeholder="Enter Email"
                  required
                  value={email}
                  onChange={(e) => {
                    setEmail(e.target.value);
                  }}
                />
              </Form.Group>
              <Form.Group>
                <Form.Label>Contact No:</Form.Label>
                <Form.Control
                  type="number"
                  placeholder="Enter 11 Digit No."
                  required
                  value={contactNo}
                  onChange={(e) => {
                    setContactNo(e.target.value);
                  }}
                />
              </Form.Group>
              <Form.Group>
                <Form.Label>Address</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Enter Address"
                  required
                  value={address}
                  onChange={(e) => {
                    setAddress(e.target.value);
                  }}
                />
              </Form.Group>
              <Form.Group>
                <Form.Label>Password:</Form.Label>
                <Form.Control
                  type="password"
                  placeholder="Enter Password"
                  required
                  value={password}
                  onChange={(e) => {
                    setPassword(e.target.value);
                  }}
                />
              </Form.Group>
              <Form.Group>
                <Form.Label>Confirm Password:</Form.Label>
                <Form.Control
                  type="password"
                  placeholder="Confirm Password"
                  required
                  value={confirmPassword}
                  onChange={(e) => {
                    setConfirmPassword(e.target.value);
                  }}
                />
              </Form.Group>
              {isActive ? (
                <Button className="mt-3" variant="danger" type="submit">
                  Submit
                </Button>
              ) : (
                <Button className="mt-3" variant="danger" disabled>
                  Submit
                </Button>
              )}
            </Form>
          </Card>
        </Col>
      </Row>
    </Container>
  ); // end of return
} // end of Register
