import { useState, useEffect } from "react";
import UsersView from "../components/UsersView";

export default function User() {
  const [users, setUsers] = useState([]);

  const getUsers = () => {
    fetch(`${process.env.REACT_APP_API_URL}/users/all-users`, {
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        setUsers(data);
      });
  };

  console.log(users);

  useEffect(() => {
    getUsers();
  }, []);

  return (
    <>
      <UsersView usersData={users} getUsers={getUsers} />
    </>
  );
} //end of User
