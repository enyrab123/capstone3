import { Carousel } from "react-bootstrap";
import FeaturedProducts from "../components/FeaturedProducts";
import { useState } from "react";

export default function Home() {
  const [index, setIndex] = useState(0);

  const handleSelect = (selectedIndex) => {
    setIndex(selectedIndex);
  };
  return (
    <>
      <Carousel className="mt-3" activeIndex={index} onSelect={handleSelect}>
        <Carousel.Item>
          <img
            className="d-block w-100 rounded"
            src="https://falconreact.prium.me/static/media/2.2001c1c3eac44ebd6423.jpg"
            alt="First slide"
          />
          <Carousel.Caption>
            <h3>Apple iMac Pro</h3>
            <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
          </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item>
          <img
            className="d-block w-100 rounded"
            src="https://falconreact.prium.me/static/media/3.ba98a1ec4092e9f3e1e8.jpg"
            alt="First slide"
          />
          <Carousel.Caption>
            <h3>Apple iPhone XS Max</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
          </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item>
          <img
            className="d-block w-100 rounded"
            src="https://falconreact.prium.me/static/media/7.28b0461293b557493c78.jpg"
            alt="First slide"
          />
          <Carousel.Caption>
            <h3>Nikon D3200 Digital DSLR Camera</h3>
            <p>
              Praesent commodo cursus magna, vel scelerisque nisl consectetur.
            </p>
          </Carousel.Caption>
        </Carousel.Item>

        <Carousel.Item>
          <img
            className="d-block w-100 rounded"
            src="https://falconreact.prium.me/static/media/5.09ac6a83faf13369156d.jpg"
            alt="First slide"
          />
          <Carousel.Caption>
            <h3>Apple Watch Series 4 44mm GPS Only</h3>
            <p>
              Praesent commodo cursus magna, vel scelerisque nisl consectetur.
            </p>
          </Carousel.Caption>
        </Carousel.Item>
      </Carousel>
      <FeaturedProducts />
    </>
  );
} // end of function Home
