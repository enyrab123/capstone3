import { useState, useEffect, useContext } from "react";
import {
  Form,
  Button,
  Card,
  Container,
  Row,
  Col,
  Image,
} from "react-bootstrap";
import { Navigate } from "react-router-dom";
import Swal from "sweetalert2";
import UserContext from "../UserContext";
import loginImg from "../img/login-img.png";

export default function Login() {
  const { user, setUser } = useContext(UserContext);

  // State hooks to store the values of the input fields
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  // State to determine whether submit button is enabled or not
  const [isActive, setIsActive] = useState(true);

  function authenticate(e) {
    // Prevents page redirection via form submission
    e.preventDefault();

    console.log("API URL:", process.env.REACT_APP_API_URL);

    fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email: email,
        password: password,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        if (data.access) {
          localStorage.setItem("token", data.access);
          retrieveUserDetails(data.access, data.id);

          setUser({
            access: localStorage.getItem("token"),
          });

          Swal.fire({
            title: "Login Successful",
            icon: "success",
            text: "Welcome to the store",
          });
        } else {
          Swal.fire({
            title: "Authentication failed",
            icon: "error",
            text: data.message,
          });
        }
      });
  } // end of authenticate

  const retrieveUserDetails = (token, id) => {
    fetch(`${process.env.REACT_APP_API_URL}/users/retrieve-user-details/`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        setUser((prevUser) => ({
          ...prevUser,
          firstName: data.firstName,
          id: id,
          isAdmin: data.isAdmin,
        }));
      });
  }; // end of retrieveUserDetails

  useEffect(() => {
    // Validation to enable submit button when all fields are populated and both passwords match
    if (email !== "" && password !== "") {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password]);

  return user.id !== null ? (
    <Navigate to="/products" />
  ) : (
    <Container className="mt-5">
      <Row className="justify-content-center">
        <Col lg={5}>
          <Image src={loginImg} fluid />
        </Col>

        <Col lg={4}>
          <Card className="border-0 p-5">
            <Form onSubmit={(e) => authenticate(e)}>
              <h1 className="my-5 text-center">Login</h1>
              <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control
                  type="email"
                  placeholder="Enter email"
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                  required
                />
              </Form.Group>

              <Form.Group controlId="password">
                <Form.Label>Password</Form.Label>
                <Form.Control
                  type="password"
                  placeholder="Password"
                  value={password}
                  onChange={(e) => setPassword(e.target.value)}
                  required
                />
              </Form.Group>

              {isActive ? (
                <Button
                  className="mt-3"
                  variant="danger"
                  type="submit"
                  id="submitBtn"
                >
                  Submit
                </Button>
              ) : (
                <Button
                  className="mt-3"
                  variant="danger"
                  type="submit"
                  id="submitBtn"
                  disabled
                >
                  Submit
                </Button>
              )}
            </Form>
          </Card>
        </Col>
      </Row>
    </Container>
  ); // end of return
} // end of function Login
