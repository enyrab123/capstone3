import { useState, useEffect, useContext } from "react";
import UserContext from "../UserContext";
import UserProductView from "../components/UserProductView";
import AdminProductView from "../components/AdminProductView";

export default function Products() {
  const { user } = useContext(UserContext);

  const [products, setProducts] = useState([]);

  const fetchData = () => {
    if (user.isAdmin) {
      fetch(`${process.env.REACT_APP_API_URL}/products/retrieve-products`, {
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      })
        .then((res) => res.json())
        .then((data) => {
          console.log(data);

          setProducts(data);
        });
    } else {
      fetch(
        `${process.env.REACT_APP_API_URL}/products/retrieve-active-products`
      )
        .then((res) => res.json())
        .then((data) => {
          console.log(data);

          setProducts(data);
        });
    }
  }; // end of fetchData

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <>
      {user.isAdmin === true ? (
        <AdminProductView productsData={products} fetchData={fetchData} />
      ) : (
        <UserProductView productsData={products} />
      )}
    </>
  ); // end of return
} // end of function Products
