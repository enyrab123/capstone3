import { useState, useEffect, useContext } from "react";
import UserContext from "../UserContext";
import { useParams, Link } from "react-router-dom";
import { Container, Row, Col, Image, Button } from "react-bootstrap";
import Swal from "sweetalert2";
import FeaturedProducts from "../components/FeaturedProducts";

export default function ProductsView() {
  const { user } = useContext(UserContext);

  const { productId } = useParams();

  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState(0);
  const [image, setimage] = useState(0);

  console.log(user.id);

  useEffect(() => {
    console.log(productId);

    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        setName(data.name);
        setDescription(data.description);
        setPrice(data.price);
        setimage(data.image);
      });
  }, [productId]);

  const addToCart = () => {
    fetch(`${process.env.REACT_APP_API_URL}/cart/${user.id}/add-to-cart`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        products: [
          {
            productId: productId,
            quantity: 1,
          },
        ],
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        if (data.success) {
          Swal.fire({
            title: "Success",
            icon: "success",
            text: `${name} is added to cart`,
          });
        } else {
          Swal.fire({
            title: "Error",
            icon: "error",
            text: data.message,
          });
        }
      });
  };

  return (
    <>
      <Container className="mt-5 mb-5">
        <Row>
          <Col lg={6}>
            <Image src={image} rounded fluid style={{ width: "40rem" }} />
          </Col>

          <Col className="ms-3">
            <h4>{name}</h4>
            <p>{description}</p>
            <h4 className="text-danger mt-3">
              &#8369;{price.toLocaleString()}
            </h4>
            <p className="mt-3">Shipping Cost: FREE</p>
            <p>
              Stock: <span className="text-success">Available</span>
            </p>

            {user.id !== null ? (
              <Button
                className="btn-sm"
                variant="danger"
                block
                onClick={addToCart}
              >
                Add to cart
              </Button>
            ) : (
              <Link className="btn btn-sm btn-danger btn-block" to="/login">
                Log in to Buy
              </Link>
            )}
          </Col>
        </Row>
      </Container>

      <hr />
      <FeaturedProducts />
    </>
  ); // end of return
} // end of function Products
