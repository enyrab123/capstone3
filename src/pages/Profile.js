import { useContext, useEffect, useState } from "react";
import UserContext from "../UserContext";
import { Navigate } from "react-router-dom";
import {
  Card,
  Col,
  Container,
  Row,
  Image,
  Form,
  Button,
} from "react-bootstrap";
import userImage from "../img/user.png";
import Swal from "sweetalert2";

export default function Profile() {
  const { user } = useContext(UserContext);

  const [userId, setUserId] = useState("");
  const [email, setEmail] = useState("");
  const [firstName, setFirstname] = useState("");
  const [lastName, setLastname] = useState("");
  const [contactNo, setContactNo] = useState("");
  const [address, setAddress] = useState("");

  const [totalOrders, setTotalOrders] = useState(0);
  const [totalAmount, setTotalAmount] = useState(0);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/retrieve-user-details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data._id);

        if (typeof data._id !== undefined) {
          setUserId(data._id);
          setEmail(data.email);
          setFirstname(data.firstName);
          setLastname(data.lastName);
          setContactNo(data.contactNo);
          setAddress(data.address);
        }
      });

    fetch(`${process.env.REACT_APP_API_URL}/orders/${user.id}/get-user-stats`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        if (data.success) {
          setTotalOrders(data.totalOrders);
          setTotalAmount(data.totalAmount);
        }
      });
  }, [user.id]);

  const updateInfo = (e, userId) => {
    e.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/users/${userId}/update-user-info`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        email: email,
        firstName: firstName,
        lastName: lastName,
        contactNo: contactNo,
        address: address,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        if (data.success === true) {
          Swal.fire({
            title: "Success!",
            icon: "success",
            text: "User Info Successfully Updated",
          });
        } else {
          Swal.fire({
            title: "Error!",
            icon: "error",
            text: "Please try again",
          });
        }
      });
  };

  return user.id === null ? (
    <Navigate to="/products" />
  ) : (
    <>
      <Container className="mt-5">
        <Row>
          <Col lg={3} xs={12}>
            <Card className="p-3 h-100">
              <div className="text-center h-100">
                <Image
                  style={{ width: "16rem", height: "16rem" }}
                  src={userImage}
                  fluid
                />
              </div>
              <div className="d-flex flex-column mt-3 justify-content-center">
                <div className="text-center">
                  <h6 className="mb-0 fw-bold text-danger">{totalOrders}</h6>
                  <p className="mb-0 small">Total Orders</p>
                </div>

                <div className="text-center mt-2">
                  <h6 className="mb-0 fw-bold text-danger">
                    &#8369;{totalAmount.toLocaleString()}
                  </h6>
                  <p className="mb-0 small">Total Spent</p>
                </div>
              </div>
            </Card>
          </Col>

          <Col lg={9} xs={12}>
            <Card className="p-3 h-100">
              <Row>
                <Col lg={6} xs={12}>
                  <Form>
                    <Form.Group className="mb-3">
                      <Form.Label>Email address</Form.Label>
                      <Form.Control
                        type="email"
                        placeholder="Enter email"
                        onChange={(e) => setEmail(e.target.value)}
                        value={email}
                      />
                    </Form.Group>

                    <Form.Group className="mb-3">
                      <Form.Label>First name</Form.Label>
                      <Form.Control
                        type="text"
                        placeholder="Enter first name"
                        onChange={(e) => setFirstname(e.target.value)}
                        value={firstName}
                      />
                    </Form.Group>
                  </Form>
                </Col>

                <Col lg={6} xs={12}>
                  <Form.Group className="mb-3">
                    <Form.Label>Phone no</Form.Label>
                    <Form.Control
                      type="tel"
                      placeholder="Enter phone no"
                      onChange={(e) => setContactNo(e.target.value)}
                      value={contactNo}
                    />
                  </Form.Group>

                  <Form.Group className="mb-3">
                    <Form.Label>Last name</Form.Label>
                    <Form.Control
                      type="text"
                      placeholder="Enter last name"
                      onChange={(e) => setLastname(e.target.value)}
                      value={lastName}
                    />
                  </Form.Group>
                </Col>
              </Row>
              <Row>
                <Col>
                  <Form.Group className="mb-3">
                    <Form.Label>Complete address</Form.Label>
                    <Form.Control
                      type="text"
                      placeholder="Enter complete address"
                      onChange={(e) => setAddress(e.target.value)}
                      value={address}
                    />
                  </Form.Group>
                </Col>
              </Row>

              <div className="d-flex justify-content-end">
                <Button
                  onClick={(e) => updateInfo(e, userId)}
                  variant="danger"
                  type="submit"
                  className="align-content-end"
                >
                  Save changes
                </Button>
              </div>
            </Card>
          </Col>
        </Row>
      </Container>
    </>
  ); //end of return
} //end of Profile
