import { useContext, useState, useEffect } from "react";
import UserContext from "../UserContext";
import CartView from "../components/CartView";

export default function Cart() {
  const { user } = useContext(UserContext);
  console.log(user.id);

  const [cart, setCart] = useState([]);

  const fetchData = () => {
    fetch(`${process.env.REACT_APP_API_URL}/cart/${user.id}/retrieve-cart`, {
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data); // Sort the data by the addedOn property

        // Check if the data variable is an array
        if (!Array.isArray(data)) {
          data = Array.from(data);
        }

        // Sort the data by the addedOn property
        data.sort((a, b) => {
          return new Date(a.addedOn) - new Date(b.addedOn);
        });

        data.reverse(); // this will sort in reverse

        setCart(data);
        console.log(cart);
      });
  }; //end of fetchData

  useEffect(() => {
    if (user && user.id) {
      fetchData();
    }
  }, [user]);

  return (
    <>
      <CartView cartData={cart} fetchData={fetchData} />   {" "}
    </>
  ); //end of return
} //end of function Cart
