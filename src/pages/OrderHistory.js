import React, { useState, useEffect, useContext } from "react";
import UserContext from "../UserContext";
import OrderHistoryView from "../components/OrderHistoryView";

export default function OrderHistory() {
  const { user } = useContext(UserContext);
  const [orders, setOrders] = useState([]);

  const fetchData = () => {
    if (user.isAdmin) {
      fetch(`${process.env.REACT_APP_API_URL}/orders/all-orders`, {
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      })
        .then((res) => res.json())
        .then((data) => {
          console.log(data);

          // Sort the data by the addedOn property
          data.sort((a, b) => {
            return new Date(a.purchasedOn) - new Date(b.purchasedOn);
          });

          data.reverse(); // this will sort in reverse

          setOrders(data);
        });
    } else {
      fetch(
        `${process.env.REACT_APP_API_URL}/orders/${user.id}/retrieve-orders`,
        {
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        }
      )
        .then((res) => res.json())
        .then((data) => {
          // Check if the data variable is an array
          if (!Array.isArray(data)) {
            data = Array.from(data);
          }

          // Sort the data by the addedOn property
          data.sort((a, b) => {
            return new Date(a.addedOn) - new Date(b.addedOn);
          });

          data.reverse();

          setOrders(data);
        })
        .catch((error) => {
          console.error("Fetch error:", error);
        });
    }
  };

  // Allows to retain orderHistory after page refresh
  useEffect(() => {
    if (user && user.id) {
      fetchData();
    }
  }, [user]);

  return orders.length > 0 ? (
    <>
      <OrderHistoryView orderData={orders} fetchData={fetchData} />
    </>
  ) : (
    <h1 className="text-center mt-5">No purchases yet</h1>
  );
}
