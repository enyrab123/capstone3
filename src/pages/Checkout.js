import UserContext from "../UserContext";
import { useContext, useState, useEffect } from "react";
import CheckoutView from "../components/CheckoutView";

export default function Checkout() {
  const { user } = useContext(UserContext);
  const [cart, setCart] = useState([]);

  const fetchData = () => {
    fetch(`${process.env.REACT_APP_API_URL}/cart/${user.id}/retrieve-cart`, {
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        // Check if the data variable is an array
        if (!Array.isArray(data)) {
          data = Array.from(data);
        }

        setCart(data);

        console.log(cart);
      });
  }; //end of fetchData

  useEffect(() => {
    fetchData();
  }, []);

  return <CheckoutView cartData={cart} fetchData={fetchData} />; //end of return
} //end of Checkout
